package application;

import java.util.ArrayList;
import ast.Function;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import syntax.Parser;
import syntax.TokenScanner;

public class MiniPlotterApp extends Application {
	/*
	 * TextFields to write function and determine interval of function
	 */
	private TextField functionField;
	private TextField plotFromField;
	private TextField plotToField;
	/*
	 * Labels for TextFields
	 */

	private Label functionLabel;
	private Label fromLabel;
	private Label toLabel;
	/*
	 * Button to plot
	 */

	private Button plotButton;
	// private RadioButton holdButton;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 * 
	 * 
	 * 
	 */

	@Override
	public void start(Stage primaryStage) throws Exception {
		BorderPane root = new BorderPane();

		Scene scene = new Scene(root, 700, 500);
		primaryStage.setTitle("MiniPlotter");
		primaryStage.setScene(scene);
		primaryStage.show();

		/*
		 * Putting textFields and the button in a HBox
		 */

		HBox hBox = new HBox();
		hBox.setPadding(new Insets(10, 5, 5, 10));

		functionField = new TextField();
		functionLabel = new Label("Function:");
		plotFromField = new TextField("0");
		fromLabel = new Label("From:");
		plotToField = new TextField(" 10");
		toLabel = new Label("To:");

		/*
		 * Spacing between buttons, text and TextFields
		 */

		hBox.setSpacing(10);

		functionField.setPrefWidth(350);
		plotFromField.setPrefWidth(50);
		plotToField.setPrefWidth(50);

		plotButton = new Button("Plot this!");

		hBox.getChildren().addAll(functionLabel, functionField, fromLabel, plotFromField, toLabel, plotToField,
				plotButton);

		/*
		 * Putting the things that makes the graph in its own HBox
		 */

		HBox lineBox = new HBox();
		/*
		 * Labelling the axes
		 */
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		xAxis.setForceZeroInRange(false);
		yAxis.setForceZeroInRange(false);
		xAxis.setLabel("x");
		yAxis.setLabel("f(x)");
		final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);
	    lineChart.setCreateSymbols(false); //hide dots


		/*
		 * Sets the plotButtons function
		 */

		plotButton.setOnAction(event -> {
			showFunction(functionField, plotFromField, plotToField, lineChart);

		});

		functionField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) { // Makes user able to press
													// enter to plot function
				plotButton.fire();
			}

		});
		

		plotFromField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) { // Makes user able to press
													// enter to plot function
				plotButton.fire();
			}

		});

		plotToField.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) { // Makes user able to press
													// enter to plot function
				plotButton.fire();
			}

		});

		lineBox.getChildren().add(lineChart);
		HBox.setHgrow(lineChart, Priority.ALWAYS); // Makes the graph change
													// size when the window
													// changes size

		/**
		 * Setting the TextFields and button at the top and the LineChart att
		 * the bottom
		 */

		root.setTop(hBox);
		root.setCenter(lineBox);

	}

	/**
	 * Method to plot the function using LineChart
	 **
	 */

	private static void showFunction(TextField functionField, TextField plotFromField, TextField plotToField,
			LineChart<Number, Number> lineChart) {
		final TokenScanner scanner = new TokenScanner();
		final Parser parser = new Parser();
		String function = functionField.getText().toLowerCase(); // toLowerCase
																	// to make
																	// it easier
																	// to input
																	// function
		if (function.equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText("Oh no!");
			alert.setContentText("No function has been entered");
			alert.showAndWait();
		} else {

			String from = plotFromField.getText();
			String to = plotToField.getText();
			try {
				Double.parseDouble(from);
				Double.parseDouble(to);

				if (Double.valueOf(from) < Double.valueOf(to)) {

					Function f = null;

					try {
						f = parser.parse(scanner.scan(function));

					} catch (Exception e) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Information");
						alert.setHeaderText("Oh no!");
						alert.setContentText(e.getMessage());
						alert.showAndWait();

					}
					ArrayList<String> errors = new ArrayList<String>();
					f.collectErrors(errors);
					if (errors.isEmpty()) {
						XYChart.Series<Number, Number> values = new XYChart.Series<Number, Number>();
						values.setName(function);
						for (double i = Double.valueOf(from); i < Double
								.valueOf(to); i += (Double.valueOf(to) - Double.valueOf(from)) / 1000) {
							try {
								values.getData().add(new XYChart.Data<Number, Number>(i, f.value(i)));

							} catch (Exception e) {
								Alert alert = new Alert(AlertType.INFORMATION);
								alert.setTitle("Information");
								alert.setHeaderText("Oh no!");
								alert.setContentText(e.getMessage());
								alert.showAndWait();
								break;
							}

						}
						lineChart.getData().clear();

						lineChart.getData().addAll(values);

					} else {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Information");
						alert.setHeaderText("Oh no!");
						alert.setContentText(errors.toString()); 
						alert.showAndWait();

					}
				} else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Information");
					alert.setHeaderText("Oh no!");
					alert.setContentText("Boundary 'From' has to be smaller than 'To'");
					alert.showAndWait();
				}
			} catch (NumberFormatException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information");
				alert.setHeaderText("Oh no!");
				alert.setContentText("Boundaries has to be numbers");
				alert.showAndWait();

			}
		}
	}

	/*
	 * Main
	 */

	public static void main(String[] args) { // k�r programmet
		Application.launch(args);

	}

}
