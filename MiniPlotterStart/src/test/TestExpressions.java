package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ast.Function;
import syntax.Parser;
import syntax.TokenScanner;

public class TestExpressions {
	private TokenScanner scanner;
	private Parser parser;

	@Before
	public void setUp() {
		scanner = new TokenScanner();
		parser = new Parser();
	}

	@Test
	public void num() {
		Function f = parseValidInput("1");
		assertEquals(1.0, f.value(0), 0.01);
		assertEquals("1.0", f.toString());
	}

	@Test
	public void add() {
		Function f = parseValidInput("-1+2");
		assertEquals(1.0, f.value(0), 0.01);
		assertEquals("-1.0 + 2.0", f.toString());
	}

	@Test
	public void add2() {
		Function f = parseValidInput("1+2+3");
		assertEquals(6.0, f.value(0), 0.01);
		assertEquals("1.0 + 2.0 + 3.0", f.toString());
	}

	@Test
	public void subtract() {
		Function f = parseValidInput("2-1");
		assertEquals(1.0, f.value(0), 0.01);
		assertEquals("2.0 - 1.0", f.toString());
	}

	@Test
	public void subtract2() {
		Function f = parseValidInput("2-1-2");
		assertEquals(-1.0, f.value(0), 0.01);
		assertEquals("2.0 - 1.0 - 2.0", f.toString());
	}

	@Test
	public void subtractAndAdd() {
		Function f = parseValidInput("2-1+2");
		assertEquals(3.0, f.value(0), 0.01);
		assertEquals("2.0 - 1.0 + 2.0", f.toString());
	}
	
	@Test
	public void multiply() {
		Function f = parseValidInput("6*2");
		assertEquals(12.0, f.value(0), 0.01);
		f = parseValidInput("cos(0)*(3+2)");
		assertEquals(5.0, f.value(0), 0.01);
	}
	
	@Test
	public void divideAndMultiply() {
		Function f = parseValidInput("6/2");
		assertEquals(3.0, f.value(0), 0.01);
		f = parseValidInput("cos(0)/(2*2+1)");
		assertEquals(0.2, f.value(0), 0.01);
	}

	@Test
	public void pi() {
		Function f = parseValidInput("pi");
		assertEquals(Math.PI, f.value(0), 0.01);
	}

	@Test
	public void sin() {
		Function f = parseValidInput("sin(0)");
		assertEquals(0, f.value(0), 0.01);
		f = parseValidInput("sin(0+pi)");
		assertEquals(0, f.value(0), 0.01);
	}
	
	@Test
	public void cos() {
		Function f = parseValidInput("cos(0)");
		assertEquals(1.0, f.value(0), 0.01);
		f = parseValidInput("cos(0+pi)");
		assertEquals(-1.0, f.value(0), 0.01);
	}
	
	@Test
	public void sqrt() {
		Function f = parseValidInput("sqrt(9)");
		assertEquals(3.0, f.value(0), 0.01);
		f = parseValidInput("sqrt(19-3)");
		assertEquals(4.0, f.value(0), 0.01);
	}
	
	@Test
	public void ln() {
		Function f = parseValidInput("ln(1)");
		assertEquals(0, f.value(0), 0.01);
		f = parseValidInput("ln(5+2)");
		assertEquals(Math.log(7), f.value(0), 0.01);
	}
	
	@Test
	public void exp() {
		Function f = parseValidInput("exp(0)");
		assertEquals(1.0, f.value(0), 0.01);
		f = parseValidInput("exp(0-2)");
		assertEquals(Math.exp(-2), f.value(0), 0.01);
	}
	
	@Test
	public void x() {
		Function f = parseValidInput("x+2");
		assertEquals(3.0, f.value(1.0), 0.01);
		f = parseValidInput("sin(x-1)");
		assertEquals(0, f.value(1), 0.01);
		f = parseValidInput("sin(1+cos((pi+x+-1))*3/3)");
		assertEquals(0, f.value(1), 0.01);
	}
	
	@Test
	public void collect() {
		Function f = parseValidInput("coh(pe)+y");
		ArrayList<String> errors = new ArrayList<String>();
		f.collectErrors(errors);
		for(String s : errors){
			System.out.println(s);
		}
	}
	

	@Test
	public void invalidSyntax() {
		parseInvalidSyntax("1+");
		parseInvalidSyntax("1++2");
		parseInvalidSyntax("1 2 3");
		parseInvalidSyntax("+1");
		parseInvalidSyntax("2 5");
		parseInvalidSyntax("5 *");
		parseInvalidSyntax("**");
		parseInvalidSyntax("j*");
		parseInvalidSyntax("(5+2");
		parseInvalidSyntax("3(5)");
		}

	private Function parseValidInput(String input) {
		try {
			return parser.parse(scanner.scan(input));
		} catch (Exception e) {
			fail(e.getMessage());
		}
		return null;
	}

	private void parseInvalidSyntax(String input) {
		try {
			parser.parse(scanner.scan(input));
			fail("Input \"" + input + "\" should result in parse error");
		} catch (Exception e) {
		}
	}
}
