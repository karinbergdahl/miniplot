package syntax;

import java.util.Iterator;
import java.util.List;

import ast.AddExpr;
import ast.DivExpr;
import ast.Expr;
import ast.FunExpr;
import ast.Function;
import ast.MultExpr;
import ast.NumExpr;
import ast.SubExpr;
import ast.XExpr;

/**
 * Parser for grammar: Expr -> Term ("+" Term)* Term -> INT
 */
public class Parser {
	private final static String EOF_TOKEN = "$EOF";
	private Iterator<String> remainingTokens;
	private String token;

	/**
	 * Parses a list of tokens and returns them as a function. Throws exception
	 * if a token cannot be parsed.
	 * 
	 * @param tokens
	 * @return Function
	 * @throws ParseException
	 */
	public Function parse(List<String> tokens) throws ParseException {
		remainingTokens = tokens.iterator();
		token = remainingTokens.hasNext() ? remainingTokens.next() : EOF_TOKEN;
		Expr e = expr();
		if (token != EOF_TOKEN) {
			throw new ParseException("Cannot parse: " + token);
		}
		return new Function(e);
	}

	private Expr expr() throws ParseException {
		Expr e = term();
		while (token.equals("+") || token.equals("-")) {
			String sign = token;
			accept();
			Expr e2 = term();
			e = sign.equals("+") ? new AddExpr(e, e2) : new SubExpr(e, e2);
		}
		return e;
	}

	private Expr term() throws ParseException {
		Expr e = factor();
		while (token.equals("*") || token.equals("/")) {
			String sign = token;
			accept();
			Expr e2 = factor();
			e = sign.equals("*") ? new MultExpr(e, e2) : new DivExpr(e, e2);
		}
		return e;
	}

	private Expr factor() throws ParseException {
		Expr e = null;
		if (isNumber()) {
			e = new NumExpr(Double.valueOf(token));
			accept();
		} else if (isId()) {
			String id = token;
			if (id.equals("pi")) {
				e = new NumExpr(Math.PI);
				accept();
				return e;
			}
			accept();
			if (token.equals("(")) {
				accept();
				e = expr();
				accept(")");
				e = new FunExpr(id, e);
			} else {
				e = new XExpr(id);
			}
		} else if (token.equals("(")) {
			accept();
			e = expr();
			accept(")");
		} else {
			throw new ParseException("Unexpected token: " + token);
		}
		return e;

	}

	private void accept() {
		token = remainingTokens.hasNext() ? remainingTokens.next() : EOF_TOKEN;
	}

	private void accept(String expected) throws ParseException {
		if (!token.equals(expected)) {
			throw new ParseException("Expected: " + expected + ", got: " + token);
		}
		accept();
	}

	private boolean isNumber() {
		return Character.isDigit(token.charAt(0)) || (token.charAt(0) == '-' && Character.isDigit(token.charAt(1)));
	}

	private boolean isId() {
		return Character.isLetter(token.charAt(0));
	}

	public static class ParseException extends Exception {
		private static final long serialVersionUID = 1L;

		public ParseException(String message) {
			super(message);
		}
	}
}
