package ast;

import java.util.ArrayList;

/**
 * Represents a function in a tree-structure
 */
public class Function {
	private final Expr expr;

	/**
	 * Creates a function from the given expression
	 * 
	 * @param expr
	 */
	public Function(Expr expr) {
		this.expr = expr;
	}

	/**
	 * Gives the value of the function evaluated at the point x
	 * 
	 * @param x
	 * @return value
	 */
	public double value(double x) {
		return expr.value(x);
	}

	/**
	 * Returns a String-representation of the function
	 */
	public String toString() {
		return expr.toString();
	}

	/**
	 * Searches the function-tree for any invalid variables or functions (y,
	 * cosh etc.). Any errors found is added to the list "errors".
	 * 
	 * @param errors
	 */
	public void collectErrors(ArrayList<String> errors) {
		expr.collectErrors(errors);
	}
}
