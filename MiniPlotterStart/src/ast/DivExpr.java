package ast;

import java.util.ArrayList;

public class DivExpr extends Expr {
	public final Expr left;
	public final Expr right;

	/**
	 * Creates a division-expression with the expression left as the numerator
	 * and the expression right as the denominator.
	 * 
	 * @param left
	 * @param right
	 */
	public DivExpr(Expr left, Expr right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * Returns the value of the expression evaluated in the point x
	 * 
	 * @param x
	 * @return value
	 */
	public double value(double x) {
		if(right.value(x) < 0.00001 && right.value(x) > -0.00001){
			throw new IllegalArgumentException("Division by zero is not permitted");
		}
		return left.value(x) / right.value(x);
	}

	/**
	 * Returns a string-representation of the expression
	 * 
	 * @return expression as string
	 */
	public String toString() {
		return left.toString() + "/" + right.toString();
	}

	/**
	 * Collects any errors in the expression and adds them to the list errors.
	 * 
	 * @param errors
	 */
	public void collectErrors(ArrayList<String> errors) {
		left.collectErrors(errors);
		right.collectErrors(errors);
	}

}
