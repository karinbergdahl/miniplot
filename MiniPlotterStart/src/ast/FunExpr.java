package ast;

import java.util.ArrayList;

public class FunExpr extends Expr{
	String fun;
	Expr expr;
	
	/**
	 * Creates a function-expression with the function fun and the input expr
	 * @param fun
	 * @param expr
	 */
	public FunExpr(String fun, Expr expr){
		this.fun = fun;
		this.expr = expr;
	}
	
	/**
	 * Returns the value of the expression evaluated in the point x
	 * @param x
	 * @return value
	 */
	public double value(double x) {
		switch (fun) {
		case "sin" : return Math.sin(expr.value(x));
		case "cos" : return Math.cos(expr.value(x));
		case "sqrt" : if(expr.value(x)<0.0){
			throw new IllegalArgumentException("sqrt is not applicable with negative values");
		}else{
			return Math.sqrt(expr.value(x));
		}
		case "ln" : if(expr.value(x)<=0.0){
			throw new IllegalArgumentException("ln is not applicable with zero or negative values");
		}else{
			return Math.log(expr.value(x));
		}
		case "exp" : return Math.exp(expr.value(x));
		default : return 0;
		}
	}
	
	/**
	 * Returns a string-representation of the expression
	 * @return expression as string
	 */
	public String toString(){
		return fun + "(" + expr.toString() + ")";
	}
	
	/**
	 * Collects any errors in the expression and adds them to the list errors.
	 * @param errors
	 */
	public void collectErrors(ArrayList<String> errors){
		if(!fun.equals("sin") && !fun.equals("cos") && !fun.equals("sqrt") && !fun.equals("ln") && !fun.equals("exp")){
			errors.add("Felaktig funktion: " + fun);
		}
		expr.collectErrors(errors);
	}

}
