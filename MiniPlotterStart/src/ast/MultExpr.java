package ast;

import java.util.ArrayList;

public class MultExpr extends Expr {
	private final Expr left;
	private final Expr right;
	
	/**
	 * Creates an expression that multiplicates the expressions left and right
	 * @param left
	 * @param right
	 */
	public MultExpr(Expr left, Expr right) {
		this.left = left;
		this.right = right;
	}
	
	/**
	 * Returns the value of the expression evaluated in the point x
	 * @param x
	 * @return value
	 */
	public double value(double x) {
		return left.value(x) * right.value(x);
	}
	
	/**
	 * Returns a string-representation of the expression
	 * @return expression as string
	 */
	public String toString() {
		return left.toString() + "*" + right.toString();
	}
	
	/**
	 * Collects any errors in the expression and adds them to the list errors.
	 * @param errors
	 **/
	public void collectErrors(ArrayList<String> errors){
		left.collectErrors(errors);
		right.collectErrors(errors);
	}
}
