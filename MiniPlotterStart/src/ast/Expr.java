package ast;

import java.util.ArrayList;

public abstract class Expr {
	
	/**
	 * Computes the value of the expression evaluated in the point x
	 * @param x
	 * @return value
	 */
	public abstract double value(double x);
	
	/**
	 * Adds any errors in variables or functions to the list errors
	 * @param errors
	 */
	public abstract void collectErrors(ArrayList<String> errors);
}
