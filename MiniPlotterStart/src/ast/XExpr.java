package ast;

import java.util.ArrayList;

public class XExpr extends Expr {
	String variable;
	
	/**
	 * Creates an expression of the variable
	 * @param variable
	 */
	public XExpr(String variable){
		this.variable = variable;
	}
	
	/**
	 * Returns the value of the expression evaluated in the point x
	 * @param x
	 * @return value
	 */
	public double value(double x){
		return x;
	}
	
	/**
	 * Returns a string-representation of the expression
	 * @return expression as string
	 */
	public String toString(){
		return "x";
	}
	
	/**
	 * Collects any errors in the expression and adds them to the list errors.
	 * @param errors
	 */
	public void collectErrors(ArrayList<String> errors){
		if(!variable.equals("x")){
			errors.add("Felaktig variabel: " + variable);
		}
	}

}
