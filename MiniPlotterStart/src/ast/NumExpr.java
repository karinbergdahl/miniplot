package ast;

import java.util.ArrayList;

public class NumExpr extends Expr {
	private final double value;
	
	/**
	 * Creates an expression of the number value
	 * @param value
	 */
	public NumExpr(double value) {
		this.value = value;
	}
	
	/**
	 * Returns the value of the number
	 * @param x
	 * @return value
	 */
	public double value(double x) {
		return value;
	}

	/**
	 * Returns a string-representation of the expression
	 * @return expression as string
	 */
	public String toString() {
		return String.valueOf(value);
	}
	
	/**
	 * Collects any errors in the expression and adds them to the list errors.
	 * @param errors
	 */
	public void collectErrors(ArrayList<String> errors){
	}
}
